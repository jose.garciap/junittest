package testing;

import object.BasicMoto;

public class JunitTesting {

	//AssertEquals
	public int countA (String word) {
		int count = 0;
		for(int i = 0; i < word.length(); i++) {
			if(word.charAt(i)=='a'|| word.charAt(i)=='A') {
				count++;
			}
		}
		return count;
	}
	
	
	//AssertTrue/False
	public boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
	
	//AssertArrayEquals
	public int [] returnNumberArrayConcat(int numberIn) {
		int[] numbers = {1,2,3};
		if(numberIn > 3) {
			numbers[0] = numberIn;
		}
		return numbers;
	}
	
	//AsserNotNull
	public Object intializeMoto(String confirm) {
		BasicMoto myMoto = null;
		if(confirm.equalsIgnoreCase("yes")) {
			myMoto = new BasicMoto();
		}
		return myMoto;
	}
	
} 

