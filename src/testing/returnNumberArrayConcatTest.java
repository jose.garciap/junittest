package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class returnNumberArrayConcatTest {

	@Test
	void test() {
		JunitTesting test = new JunitTesting();
		int[] numbers = {1,2,3};
		int[] output = test.returnNumberArrayConcat(1);
		assertArrayEquals(numbers, output);
	}

}
